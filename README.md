# Laravel Installer helper #

Package to handle custom composer installer actions
========================
## Installation

composer require tshevchenko/installer

add functions to the root composer.json scripts
 ```
     "scripts": {
         "post-autoload-dump": [
             "Tshevchenko\\Installer\\Installer::postAutoloadDump"
         ],
         "post-package-install": [
             "Tshevchenko\\Installer\\Installer::postInstallEvent"
         ],
         "pre-package-uninstall": [
             "Tshevchenko\\Installer\\Installer::preUninstallEvent"
         ]
     }
 ```

## Using in the packages

package type="tshevchenko-package"

add needed actions to the package composer.json extra 
```
    "extra": {
        "Tshevchenko\\Installer\\Installer::postInstallEvent": {
            "callArtisan": [
                [
                    "migrate",
                    "test-command:test testOption",
                    "vendor:publish --tag=public --force"
                ]
            ]
        },
        "Tshevchenko\\Installer\\Installer::preUninstallEvent": {
            "callArtisan": [
                [
                    "migrate:rollback"
                ]
            ]
        }
    }
```