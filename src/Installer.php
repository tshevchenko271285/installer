<?php

namespace Tshevchenko\Installer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;
use Composer\Util\Filesystem;
use Composer\Repository\InstalledRepositoryInterface;
use Illuminate\Support\Facades\Artisan;

class Installer extends LibraryInstaller
{
    const TMP_EXTRA_FILE = 'tshevchenko/tmp_extra.php';

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'tshevchenko-package' === $packageType;
    }

    /**
     * Add info from [extra] to the TMP_EXTRA_FILE.
     *
     * @param array $extra
     * @param string $vendorDir
     * @return void
     */
    protected static function addTmpExtra($extra, $vendorDir)
    {
        $packages = static::loadTmpExtra($vendorDir);
        $packages[] = $extra;
        static::saveTmpExtra($packages, $vendorDir);
    }

    /**
     * Load [extra] info from TMP_EXTRA_FILE.
     *
     * @param string $vendorDir
     * @return array
     */
    protected static function loadTmpExtra($vendorDir)
    {
        $file = $vendorDir . '/' . static::TMP_EXTRA_FILE;
        if (!is_file($file)) {
            return [];
        }
        // invalidate opcache of extensions.php if exists
        if (function_exists('opcache_invalidate')) {
            //оставил @, т.к. эти ошибки нам не нужны
            @opcache_invalidate($file, true);
        }
        return require($file);
    }

    /**
     * Save TMP_EXTRA_FILE.
     *
     * @param array $packages
     * @param string $vendorDir
     * @return void
     */
    protected static function saveTmpExtra(array $packages, $vendorDir)
    {
        $file = $vendorDir . '/' . static::TMP_EXTRA_FILE;
        if (!file_exists(dirname($file))) {
            mkdir(dirname($file), 0777, true);
        }
        $array = str_replace("'<vendor-dir>", '$vendorDir . \'', var_export($packages, true));
        file_put_contents($file, "<?php\n\n\$vendorDir = dirname(__DIR__);\n\nreturn $array;\n");
        // invalidate opcache of extensions.php if exists
        if (function_exists('opcache_invalidate')) {
            //оставил @, т.к. эти ошибки нам не нужны
            @opcache_invalidate($file, true);
        }
    }

    /**
     * Clear TMP_EXTRA_FILE.
     *
     * @param string $vendorDir
     * @return void
     */
    protected static function clearTmpExtra($vendorDir)
    {
        static::saveTmpExtra([], $vendorDir);
    }

    /**
     * Special method to run tasks defined in `[extra][$extraKey]` key in `composer.json`
     *
     * @param Event $event
     * @param string $extraKey
     * @return void
     */
    protected static function runEventCommands($event, $extraKey)
    {
        $params = $event->getOperation()->getPackage()->getExtra();
        if (isset($params[$extraKey]) && is_array($params[$extraKey])) {
            foreach ($params[$extraKey] as $method => $args) {
                call_user_func_array([__CLASS__, $method], (array) $args);
            }
        }
    }

    /**
     * Call php artisan commands.
     *
     * @param array $commands
     * @return void
     */
    public static function callArtisan(array $commands)
    {
        foreach ($commands as $command) {
            exec('php artisan ' . $command);
        }
    }

    /**
     * Special method to run tasks defined in
     * `[extra][Tshevchenko\Installer\Installer::postInstallEvent]` key in `composer.json`
     *
     * @param Event $event
     * @return void
     */
    public static function postInstallEvent($event)
    {
        $extra = $event->getOperation()->getPackage()->getExtra();
        if (isset($extra[__METHOD__]) && is_array($extra[__METHOD__])) {
            $vendorDir = static::getVendorDir($event);
            static::addTmpExtra($extra[__METHOD__], $vendorDir);
        }
    }

    /**
     * Special method to run tasks defined in
     * `[extra][Tshevchenko\Installer\Installer::preUninstallEvent]` key in `composer.json`
     *
     * @param Event $event
     * @return void
     */
    public static function preUninstallEvent($event)
    {
        static::runEventCommands($event, __METHOD__);
    }

    /**
     * Special method to run tasks defined in
     * `[extra][Tshevchenko\Installer\Installer::postAutoloadDump]` key in `composer.json`
     *
     * @param Event $event
     * @return void
     */
    public static function postAutoloadDump($event)
    {
        $vendorDir = static::getVendorDir($event);
        try {
            $extra = static::loadTmpExtra($vendorDir);
            static::clearTmpExtra($vendorDir);
            foreach ($extra as $package) {
                foreach ($package as $method => $args) {
                    call_user_func_array([__CLASS__, $method], (array)$args);
                }
            }
        } catch (\Exception $e) {
            static::clearTmpExtra($vendorDir);
        }
    }

    /**
     * return vendor dir from $event
     *
     * @param Event $event
     * @return string
     */
    public static function getVendorDir($event)
    {
        return rtrim($event->getComposer()->getConfig()->get('vendor-dir'), '/');
    }
}
