<?php

namespace Tshevchenko\Installer\Facades;

use Illuminate\Support\Facades\Facade;

class Installer extends Facade
{
    /**
     * {@inheritdoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'installer';
    }
}
