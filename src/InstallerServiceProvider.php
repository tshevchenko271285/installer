<?php
namespace Tshevchenko\Installer;

use Illuminate\Support\ServiceProvider;

class InstallerServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $this->app->bind('installer', function () {
            return new InstallerPlugin();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function boot()
    {

    }
}
